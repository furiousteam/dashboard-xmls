[
    {
        "key": "Inventory_Market Sellout Trends by Network",
        "type": "list_value_with_change",
        "title": "Sellout Trends by Network",
        "width": 12,
        "params": {
            "postfix": "%",
            "show_percent": true,
            "date_modification": "same_date_prev_1_year",
            "elements_in_page": 12
        },
        "tooltip": "Change in sellout compared to same period year over year",
        "calculations": [
            {
                "method": "raw_query",
                "params": {
                    "pipeline_1": {
                        "$group": {
                            "_id": "$network",
                            "sold": {
                                "$sum": "$sold"
                            },
                            "capacity": {
                                "$sum": "$capacity"
                            }
                        }
                    },
                    "pipeline_2": {
                        "$project": {
                            "_id": "$_id",
                            "value": {
                                "$cond": [
                                    {
                                        "$eq": [
                                            "$capacity",
                                            0
                                        ]
                                    },
                                    0,
                                    {
                                        "$divide": [
                                            {
                                                "$multiply": [
                                                    "$sold",
                                                    100
                                                ]
                                            },
                                            "$capacity"
                                        ]
                                    }
                                ]
                            }
                        }
                    },
                    "pipeline_3": {
                        "$sort": {
                            "_id": 1
                        }
                    },
                    "collection_name": "capacity_vs_sold",
                    "match_market_id": "##ddl_hier_selection##raw##data_label"
                }
            }
        ]
    },
    {
        "key": "Inventory_Market Sellout by Date YoY",
        "type": "line_chart",
        "title": "Sellout by Date YoY",
        "width": 12,
        "params": {
            "y_min": 0,
            "postfix": "%",
            "show_legend": false
        },
        "tooltip": "Percent of sold inventory to available inventory by date year over year",
        "calculations": [
            {
                "method": "arithmetic_ops_on_two_col_aggr",
                "params": {
                    "sort_by": "*key",
                    "series_field": "Spot Sellout",
                    "data_column_1": "sold",
                    "data_column_2": "capacity",
                    "data_factor_1": 100,
                    "data_factor_2": 1,
                    "collection_name": "capacity_vs_sold",
                    "group_by_column": "date",
                    "match_market_id": "##ddl_hier_selection##raw##data_label",
                    "arithmetic_operator": "divide"
                }
            },
            {
                "method": "arithmetic_ops_on_two_col_aggr",
                "params": {
                    "sort_by": "*key",
                    "series_field": "Previous Year Spot Sellout",
                    "data_column_1": "sold",
                    "data_column_2": "capacity",
                    "data_factor_1": 100,
                    "data_factor_2": 1,
                    "relative_date": "year_ago",
                    "collection_name": "capacity_vs_sold",
                    "group_by_column": "date",
                    "match_market_id": "##ddl_hier_selection##raw##data_label",
                    "arithmetic_operator": "divide"
                }
            }
        ]
    },
    {
        "key": "Inventory_Market Market Inventory",
        "type": "title_widget",
        "title": "Market Inventory",
        "width": 12,
        "calculations": [
            {
                "method": "empty_data",
                "params": {}
            }
        ]
    },
    {
        "key": "Inventory_Market Impressions by Date YoY",
        "type": "line_chart",
        "title": "Impressions by Date YoY",
        "width": 12,
        "params": {
            "y_min": 0,
            "show_legend": false
        },
        "tooltip": "Impressions by date current period and year ago",
        "calculations": [
            {
                "method": "sum_single_column_group_by",
                "params": {
                    "sort_by": "*key",
                    "data_column": "impressions",
                    "series_field": "Impressions",
                    "collection_name": "capacity_vs_sold",
                    "group_by_column": "date",
                    "match_market_id": "##ddl_hier_selection##raw##data_label"
                }
            },
            {
                "method": "sum_single_column_group_by",
                "params": {
                    "sort_by": "*key",
                    "data_column": "impressions",
                    "series_field": "Previous Year Impressions",
                    "relative_date": "year_ago",
                    "collection_name": "capacity_vs_sold",
                    "group_by_column": "date",
                    "match_market_id": "##ddl_hier_selection##raw##data_label"
                }
            }
        ]
    },
    {
        "key": "Inventory_Market Sellout by Zone by Week",
        "type": "flexible_grid",
        "title": "Sellout by Zone by Week",
        "width": 12,
        "params": {
            "threshold": {
                "##weekly_data.sellout": {
                    "max": 0.5,
                    "min": 0
                }
            },
            "column_names": "_id.zone:\"Zone\",##weekly_data.date.sellout",
            "column_width": {
                "Zone": 170,
                "##weekly_data.sellout": 90
            },
            "column_filter": {
                "##weekly_data.sellout": "percentage:0:100"
            },
            "column_pinned": {
                "Zone": "left"
            },
            "column_search": {
                "Zone": "true"
            },
            "column_tooltip": {},
            "show_total_line": false,
            "sort_data_columns": {
                "##weekly_data.sellout": "true"
            },
            "column_text_alignment": {
                "Zone": "left",
                "##weekly_data.sellout": "right"
            }
        },
        "calculations": [
            {
                "method": "raw_query",
                "params": {
                    "pipeline_1": {
                        "$group": {
                            "_id": {
                                "date": "$date",
                                "zone": "$zone"
                            },
                            "sold": {
                                "$sum": "$sold"
                            },
                            "capacity": {
                                "$sum": "$capacity"
                            }
                        }
                    },
                    "pipeline_2": {
                        "$group": {
                            "_id": {
                                "zone": "$_id.zone"
                            },
                            "weekly_data": {
                                "$push": {
                                    "date": "$_id.date",
                                    "sellout": {
                                        "$cond": [
                                            {
                                                "$eq": [
                                                    "$capacity",
                                                    0
                                                ]
                                            },
                                            0,
                                            {
                                                "$divide": [
                                                    "$sold",
                                                    "$capacity"
                                                ]
                                            }
                                        ]
                                    }
                                }
                            }
                        }
                    },
                    "pipeline_3": {
                        "$sort": {
                            "_id": 1
                        }
                    },
                    "collection_name": "capacity_vs_sold",
                    "match_market_id": "##ddl_hier_selection##raw##data_label"
                }
            }
        ]
    },
    {
        "key": "Inventory_Market Bonus Spots Usage",
        "type": "flexible_grid",
        "title": "Bonus Spots Usage",
        "width": 12,
        "params": {
            "column_names": "_id.network:\"Network\",_id.AE:\"AE\",_id.advertiser:\"Advertiser\",sold:\"Spots\"",
            "column_filter": {},
            "column_search": {
                "AE": "true",
                "Network": "true",
                "Advertiser": "true"
            },
            "column_tooltip": {},
            "show_total_line": false,
            "column_text_alignment": {
                "AE": "left",
                "Spots": "right",
                "Network": "left",
                "Advertiser": "left"
            }
        },
        "tooltip": "Usage of bonus spots",
        "calculations": [
            {
                "method": "raw_query",
                "params": {
                    "pipeline_3": {
                        "$sort": {
                            "_id": 1
                        }
                    },
                    "collection_name": "sales_data",
                    "match_market_id": "##ddl_hier_selection##raw##data_label",
                    "pipeline_1": {
                        "$match": {
                            "spot_type": "Bonus"
                        }
                    },
                    "pipeline_2": {
                        "$group": {
                            "_id": {
                                "AE": "$ae_name",
                                "network": "$network",
                                "advertiser": "$advertiser"
                            },
                            "sold": {
                                "$sum": "$sold"
                            }
                        }
                    }
                }
            }
        ]
    }
]